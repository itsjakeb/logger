declare module 'logger' {

  export class Logger {
    constructor(name: string, options: LoggerOptions);

    public name: string;
    public options: LoggerOptions;
    public extensions: Object;
    public history: Array<string>;

    private addToHistory(text: string) : void;
    private replaceSensitive(string: string) : string;
    private log(name: string, type: string, text: string) : string;

    public getLog() : string;
    public info(text: string) : string;
    public warn(text: string) : string;
    public debug(text : string) : string;
    public error(text : string) : string;
    public getExtension(name: string) : LoggerExtension;
  }

  interface LoggerOptions {
    sensitive: Array<string>;
    sentry?: any;
  }

  export class LoggerExtension {
    constructor(baseLogger: Logger, name: string);

    public info(text: string) : string;
    public warn(text: string) : string;
    public debug(text: string) : string;
    public error(text: string) : string;
    public getExtension(name: string) : LoggerExtension;
  }

}